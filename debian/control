Source: pyphen
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Scott Kitterman <scott@kitterman.com>,
           Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Build-Depends: debhelper (>= 11),
               dh-python,
               hyphen-nl,
               python,
               python-pytest,
               python-setuptools,
               python3,
               python3-pytest,
               python3-setuptools
Vcs-Git: https://salsa.debian.org/python-team/modules/pyphen.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/pyphen
Standards-Version: 4.3.0
Homepage: https://pyphen.org/

Package: python-pyphen
Architecture: all
Depends: hyphen-en-us | hyphen-hyphenation-patterns,
         ${misc:Depends},
         ${python:Depends}
Description: Python hyphenation module
 Pyphen is a pure Python module to hyphenate text using existing Hunspell
 hyphenation dictionaries. This module is a fork of python-hyphenator, written
 by Wilbert Berendsen.  It uses Debian system hyphenation packages (package
 hyphen-(language code) rather than the embedded set provided by upstream.
 .
 This is the Python (2) version of the package.

Package: python3-pyphen
Architecture: all
Depends: hyphen-en-us | hyphen-hyphenation-patterns,
         ${misc:Depends},
         ${python3:Depends}
Description: Python 3 hyphenation module
 Pyphen is a pure Python 3 module to hyphenate text using existing Hunspell
 hyphenation dictionaries. This module is a fork of python-hyphenator, written
 by Wilbert Berendsen.  It uses Debian system hyphenation packages (package
 hyphen-(language code) rather than the embedded set provided by upstream.
 .
 This is the Python 3 version of the package.
